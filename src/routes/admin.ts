import { Router } from "express";
import admin from "../service/admin";
import * as ExcelJS from "exceljs";

const routerAdmin = Router();
routerAdmin.post("/login", async (req, res, next) => {
  // TODO add request body validation middleware
  // I omitted this to not make the tutorial too complex
  try {
    const adminRes = await admin.logIn(req.body.userName, req.body.password);
    if (!adminRes) {
      res.send({
        message: "user not found",
      });
    } else if (adminRes.password !== JSON.stringify(req.body.password)) {
      res.send({
        message: "wrong password",
      });
    } else {
      res.send({
        data: adminRes,
        status_code: 201,
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json("something went wrong");
  }
});

routerAdmin.get("/candidatesTest", async (req, res, next) => {
  // TODO add request body validation middleware
  // I omitted this to not make the tutorial too complex
  try {
    const candidateRes = await admin.getAllCandidatesTest();
    res.send(candidateRes);
  } catch (err) {
    console.log(err);
    res.status(500).json("something went wrong");
  }
});

routerAdmin.get("/exportData", async (req, res, next) => {
  try {
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet("Sheet 1", {
      properties: { tabColor: { argb: "FF00FF00" }, outlineLevelCol: 1 },
    });
    const candidateRes = await admin.getAllCandidatesTest();
    const transformedData = candidateRes.map((test) => ({
      id: test.id,
      candidate: test.candidate,
      user_id: test.user_id,
      question_id: test.question_id,
      passed: test.passed[0] === 1, // Chuyển buffer thành boolean
    }));
    const fileExport = [
      {
        id: "id",
        candidate: "candidate",
        user_id: "user_id",
        question_id: "question_id",
        passed: "passed",
      },
      ...transformedData,
    ];
    fileExport.forEach((row) => {
      worksheet.addRow(Object.values(row));
    });
    const row = worksheet.getRow(1);
    row.outlineLevel = 1;
    row.height = 42.5;
    row.eachCell((cell) => {
      cell.alignment = {
        vertical: "middle",
        horizontal: "center",
      };
      if (cell.value) {
        cell.font = {
          color: { argb: "FFFFFFFF" },
          bold: true,
        };
        cell.border = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
        cell.fill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "FF9999" },
        };
        const column = worksheet.getColumn(cell.col);
        column.width = 20;
      }
    });
    workbook.xlsx
      .writeBuffer()
      .then((buffer) => {
        res.setHeader("Content-Disposition", "attachment; filename=data.xlsx");
        res.setHeader(
          "Content-Type",
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        );

        res.send(buffer);
      })
      .catch((error) => {
        console.log("Đã xảy ra lỗi khi xuất file Excel:", error);
        res.status(500).send("Lỗi server");
      });
  } catch (error) {
    console.log("Đã xảy ra lỗi khi truy xuất dữ liệu:", error);
    res.status(500).send("Lỗi server");
  }
});
export default routerAdmin;
