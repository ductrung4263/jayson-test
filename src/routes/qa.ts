import { Router } from "express";
import qa from "../service/qa";

const routerQA = Router();
routerQA.post("/create", async (req, res, next) => {
  // TODO add request body validation middleware
  // I omitted this to not make the tutorial too complex
  try {
    await qa.createQA(
      req.body.question,
      req.body.correct_answer,
      req.body.list_answer,
      req.body.type_question
    );
    res.send({
      message: "create success",
    });
  } catch (err) {
    console.log(err);
    res.status(500).json("something went wrong");
  }
});

export default routerQA;
