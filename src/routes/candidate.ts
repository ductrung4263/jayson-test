import { Router } from "express";
import CandidateService from "../service/candidate";

const routerCandidate = Router();
routerCandidate.post("/login", async (req, res, next) => {
  // TODO add request body validation middleware
  // I omitted this to not make the tutorial too complex
  try {
    const candidateRes = await CandidateService.logIn(req.body.userName);
    if (!candidateRes) {
      res.send({
        message: "user not found",
      });
    } else if (candidateRes.password !== JSON.stringify(req.body.password)) {
      res.send({
        message: "wrong password",
      });
    } else {
      res.send({
        data: candidateRes,
        status_code: 201,
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json("something went wrong");
  }
});

routerCandidate.post("/register", async (req, res, next) => {
  // TODO add request body validation middleware
  // I omitted this to not make the tutorial too complex
  try {
    const candidateRes = await CandidateService.register(
      req.body.userName,
      req.body.password,
      req.body.name
    );
    res.send(candidateRes);
  } catch (err) {
    console.log(err);
    res.status(500).json("something went wrong");
  }
});

routerCandidate.post("/test", async (req, res, next) => {
  // TODO add request body validation middleware
  // I omitted this to not make the tutorial too complex
  try {
    const candidateRes = await CandidateService.test(
      req.body.id,
      req.body.answer,
      req.body.userId,
      req.body.question_id,
      req.body.candidate
    );
    res.send(candidateRes);
  } catch (err) {
    console.log(err);
    res.status(500).json("something went wrong");
  }
});

routerCandidate.post("/reviewTest", async (req, res, next) => {
  // TODO add request body validation middleware
  // I omitted this to not make the tutorial too complex
  try {
    const candidateRes = await CandidateService.reviewTest(
      req.body.userId,
      req.body.question_test_id
    );
    res.send(candidateRes);
  } catch (err) {
    console.log(err);
    res.status(500).json("something went wrong");
  }
});
export default routerCandidate;
