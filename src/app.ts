import * as express from "express";
import router from "./routes";
import routerAdmin from "./routes/admin";
import routerQA from "./routes/qa";
import routerCandidate from "./routes/candidate";

const app = express();
app.use(express.json());
app.use(router);
app.use("/admin", routerAdmin);
app.use("/qa", routerQA);
app.use("/candidate", routerCandidate);

export default app;
