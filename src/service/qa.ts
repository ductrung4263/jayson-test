import { getRepository } from "typeorm";
import { QA } from "../entity/QA";

class qaService {
  async createQA(
    question: string,
    correct_answer: string,
    list_answer: string,
    type_question: string
  ) {
    const qaRepo = await getRepository(QA);
    const qa = new QA();
    qa.question = question;
    qa.correct_answer = correct_answer;
    qa.list_answer = list_answer;
    qa.type_question = type_question;
    return qaRepo.save(qa);
  }
}

export default new qaService();
