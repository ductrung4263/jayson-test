import { getRepository } from "typeorm";
import { Candidate } from "../entity/Candidate";
import { QA } from "../entity/QA";
import { Test } from "../entity/test";

class CandidateService {
  async logIn(userName: string) {
    const adminRepo = await getRepository(Candidate)
      .createQueryBuilder("candidate")
      .where("candidate.username = :username", { username: userName })
      .getOne();
    return adminRepo;
  }
  async register(userName: string, password: string, name: string) {
    const candidate = await getRepository(Candidate);
    const candidateQuery = await candidate
      .createQueryBuilder("candidate")
      .where("candidate.username = :username", { username: userName })
      .getOne();
    if (candidateQuery) {
      return {
        message: "user existed",
      };
    } else {
      const user = new Candidate();
      user.userName = userName;
      user.password = password;
      user.name = name;
      candidate.save(user);
      return {
        message: "register success",
      };
    }
  }
  async test(
    id: string,
    answer: string,
    userId: number,
    question_id: number,
    candidate: string
  ) {
    const qa = await getRepository(QA)
      .createQueryBuilder("qa")
      .where("qa.id = :id", { id: id })
      .getOne();

    const candidateQuery = await getRepository(Candidate)
      .createQueryBuilder("candidate")
      .where("candidate.id = :id", { id: userId })
      .getOne();
    if (!qa) {
      return {
        message: "question is not exist",
      };
    } else if (!candidateQuery) {
      return {
        message: "user is not exist",
      };
    } else {
      const testRepo = await getRepository(Test);

      const testQuery = await testRepo
        .createQueryBuilder("test")
        .where("test.question_id = :question_id", { question_id })
        .getOne();
      if (!testQuery) {
        const test = new Test();
        test.candidate = candidate;
        test.user_id = userId;
        test.question_id = question_id;
        if (answer == qa.correct_answer) {
          test.passed = 1;
          test.number_correct_answer = test.number_correct_answer
            ? test.number_correct_answer + 1
            : 0;
        } else {
          test.passed = 0;
          test.number_correct_answer =
            test.number_correct_answer > -1
              ? test.number_correct_answer - 1
              : 0;
        }
        testRepo.save(test);
        return {
          message: "Thank you for doing our tests",
        };
      } else {
        return {
          message: "you did the test",
        };
      }
    }
  }
  async reviewTest(userId: number, question_test_id: number) {
    const user = await getRepository(Candidate)
      .createQueryBuilder("candidate")
      .where("candidate.id = :id", { id: userId })
      .getOne();
    const testRepo = await getRepository(Test)
      .createQueryBuilder("test")
      .where("test.user_id = :id AND test.question_id = :questionId", {
        id: userId,
        questionId: question_test_id,
      })
      .getOne();
    console.log("testRepo ", testRepo);
    if (!user) {
      return {
        message: "user is not exist",
      };
    } else {
      if (!testRepo) {
        return {
          message: "you don't have permission to see the results of the test",
        };
      } else {
        return {
          data: testRepo,
        };
      }
    }
  }
}

export default new CandidateService();
