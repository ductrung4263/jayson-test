import { getRepository } from "typeorm";
import { Admin } from "../entity/Admin";
import { Test } from "../entity/test";

class AdminService {
  async logIn(userName: string, password: string) {
    const adminRepo = await getRepository(Admin)
      .createQueryBuilder("admin")
      .where("admin.username = :username", { username: userName })
      .getOne();
    return adminRepo;
  }
  async getAllCandidatesTest() {
    const candidateRepo = await getRepository(Test)
      .createQueryBuilder("test")
      .getMany();
    return candidateRepo || [];
  }
}

export default new AdminService();
