import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Test {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  candidate: string;

  @Column()
  user_id: number;

  @Column()
  question_id: number;

  @Column()
  passed: number;
  @Column()
  number_correct_answer: number;
}
