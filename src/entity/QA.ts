import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class QA {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  question: string;

  @Column()
  correct_answer: string;

  @Column()
  list_answer: string;

  @Column()
  type_question: string;
}
